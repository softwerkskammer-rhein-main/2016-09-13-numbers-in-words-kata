package com.company;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by ibahchevanski on 13.09.16.
 */
public class WordsIntoNumbersTest {
    @Test
    public void test() throws Exception {
        assertTrue(true);

    }

    @Test
    public void testZeroDollars() throws Exception {
        WordsIntoNumbers wordsIntoNumbers = new WordsIntoNumbers();

        int digit = wordsIntoNumbers.convert("zero");
        Assert.assertEquals(0, digit);
    }

    @Test
    public void testAnyNumber() throws Exception {
        WordsIntoNumbers wordsIntoNumbers = new WordsIntoNumbers();

        int digit = wordsIntoNumbers.convert("nine");
        Assert.assertEquals(9, digit);

    }
    @Test
    public void testDigitsOne() throws Exception {
        WordsIntoNumbers wordsIntoNumbers = new WordsIntoNumbers();

        Assert.assertEquals(0, wordsIntoNumbers.convert("zero"));
        Assert.assertEquals(1, wordsIntoNumbers.convert("one"));
        Assert.assertEquals(2, wordsIntoNumbers.convert("two"));
        Assert.assertEquals(3, wordsIntoNumbers.convert("three"));
        Assert.assertEquals(4, wordsIntoNumbers.convert("four"));
        Assert.assertEquals(5, wordsIntoNumbers.convert("five"));
        Assert.assertEquals(6, wordsIntoNumbers.convert("six"));
        Assert.assertEquals(7, wordsIntoNumbers.convert("seven"));
        Assert.assertEquals(8, wordsIntoNumbers.convert("eight"));
        Assert.assertEquals(9, wordsIntoNumbers.convert("nine"));

    }
    @Test
    public void testDigitsTwo() throws Exception {
        WordsIntoNumbers wordsIntoNumbers = new WordsIntoNumbers();

        Assert.assertEquals(10, wordsIntoNumbers.convert("ten"));
        Assert.assertEquals(11, wordsIntoNumbers.convert("eleven"));
        Assert.assertEquals(19, wordsIntoNumbers.convert("nineteen"));
    }

    @Test
    public void testDigitTwoEven() throws Exception {
        WordsIntoNumbers wordsIntoNumbers = new WordsIntoNumbers();
        Assert.assertEquals(20, wordsIntoNumbers.convert("twenty"));
        Assert.assertEquals(30, wordsIntoNumbers.convert("thirty"));

    }

    @Test
    public void testDigitTwoComposite() throws Exception {
        WordsIntoNumbers wordsIntoNumbers = new WordsIntoNumbers();
        Assert.assertEquals(21, wordsIntoNumbers.convert("twenty one"));
        Assert.assertEquals(22, wordsIntoNumbers.convert("twenty two"));
        Assert.assertEquals(99, wordsIntoNumbers.convert("ninety nine"));

    }
}
