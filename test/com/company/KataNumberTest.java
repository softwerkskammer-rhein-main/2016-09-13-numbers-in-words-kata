package com.company;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class KataNumberTest {

    private KataNumber kataNumber;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {256, "Two Hundred Fifty Six"},
                {11, "Eleven"},
                {19, "Nineteen"},
                {0, "Zero"}
        });
    }

    private int input;

    private String expected;

    public KataNumberTest(int input, String expected) {
        this.input = input;
        this.expected = expected;
    }

    @Before
    public void setUp() {
        kataNumber = new KataNumber();
    }

    @Test
    public void doKataNumberTest() {
        String output = kataNumber.doKataNumber(input);
        assertEquals(output, expected);
    }

}
