package com.company;

import java.util.HashMap;

/**
 * Created by ibahchevanski on 13.09.16.
 */
public class WordsIntoNumbers {

    private static HashMap<String,Integer> numbers = new HashMap<>();

    public WordsIntoNumbers() {
        numbers.put("zero",0);
        numbers.put("one",1);
        numbers.put("two",2);
        numbers.put("three",3);
        numbers.put("four",4);
        numbers.put("five",5);
        numbers.put("six",6);
        numbers.put("seven",7);
        numbers.put("eight",8);
        numbers.put("nine",9);
        numbers.put("ten",10);

        numbers.put("eleven",11);
        numbers.put("twelve",12);
        numbers.put("thirteen",13);
        numbers.put("fourteen",14);
        numbers.put("fifteen",15);
        numbers.put("sixteen",16);
        numbers.put("seventeen",17);
        numbers.put("eighteen",18);
        numbers.put("nineteen",19);

        numbers.put("twenty",20);
        numbers.put("thirty",30);
        numbers.put("fourty",40);
        numbers.put("fifty",50);
        numbers.put("sixty",60);
        numbers.put("seventy",70);
        numbers.put("eighty",80);
        numbers.put("ninety",90);


    }

    public int convert(String number) {
        String[] parts = number.split(" ");
        int result = 0;
        for(String part : parts) {
            result += numbers.get(part);
        }
        return result;
    }
}
