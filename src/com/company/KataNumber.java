package com.company;

import java.util.HashMap;
import java.util.Map;

public class KataNumber {

    private Map<Integer, String> map =  new HashMap<>();

    public KataNumber(){
        map.put(0, "Zero");
        map.put(1, "One");
        map.put(2, "Two");
        map.put(3, "Three");
        map.put(4, "Four");
        map.put(5, "Five");
        map.put(6, "Six");
        map.put(7, "Seven");
        map.put(8, "Eight");
        map.put(9, "Nine");
        map.put(10, "Ten");
        map.put(11, "Eleven");
        map.put(12, "Twelve");
        map.put(13, "Thirteen");
        map.put(14, "Fourteen");
        map.put(15, "Fifteen");
        map.put(16, "Sixteen");
        map.put(17, "Seventeen");
        map.put(18, "Eighteen");
        map.put(19, "Nineteen");


    }

    public String doKataNumber(int number) {
        if(number<=19) {
            //int lastDigit = number % 100;
            return map.get(number);
        }
        return "Two Hundred Fifty Six";
    }
}
